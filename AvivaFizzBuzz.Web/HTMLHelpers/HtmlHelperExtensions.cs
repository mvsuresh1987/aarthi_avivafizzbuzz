﻿using System;
using System.Globalization;
using System.Text;
using System.Web.Mvc;
using AvivaFizzBuzz.Web.Models;
using System.Web.Routing;
using System.Web;

namespace AvivaFizzBuzz.Web.HTMLHelpers
{
    public static class HtmlHelperExtensions
    {
        public static RouteValueDictionary AddQueryStringParameters(this RouteValueDictionary dict)
        {
            var querystring = HttpContext.Current.Request.QueryString;

            foreach (var key in querystring.AllKeys)
                if (!dict.ContainsKey(key))
                    dict.Add(key, querystring.GetValues(key)[0]);

            return dict;
        }
    }
}