﻿using System;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AvivaFizzBuzz.Web.Startup))]
namespace AvivaFizzBuzz.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
