﻿using System.Collections.Generic;
using AvivaFizzBuzz.Web;
using System.Linq;
using AvivaFizzBuzz.Business;

namespace AvivaFizzBuzz.Web.Models
{
    public class FizzBuzzViewModel
    {
        public IEnumerable<IFizzBuzzEntry> FizzBuzzEntries { get; set; }
        
        public bool FizzBuzzEntriesExist 
        {
            get
            {
                return this.FizzBuzzEntries != null && this.FizzBuzzEntries.Count() > 0;
            }
        }
        public PagingInfo PagingInfo { get; set; }
    }
}