// TODO: Move using statements to belwo namespaces and using statements should be in order. First all system types and not system types should be in alphabetic order.
 using System;
using System.Web.Mvc;
using AvivaFizzBuzz.Repository;
using AvivaFizzBuzz.Web.Models;
using System.Linq;
using System.Web.UI;

namespace AvivaFizzBuzz.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFizzBuzzRepository fizzBuzzRepository;

// TODO: size is readonly
        public int PageSize { get; set; }

        public HomeController(IFizzBuzzRepository fizzBuzzRepository)
        {
        // TODO: Remove null check
            if (fizzBuzzRepository == null)
                throw new ArgumentNullException("fizzBuzzRepository");

            this.fizzBuzzRepository = fizzBuzzRepository;
            this.PageSize = 20;
        }

        public ActionResult List(string totalCount, int page = 1)
        {
        // TODO: Remove try catch block
            try
            {
            // TODO: Total count and page index create view model
                ViewBag.TotalCount = totalCount;
// TODO: Simplyfy logic with view model and validations.
                if (!String.IsNullOrWhiteSpace(totalCount))
                {
                    int actualTotalCount;
// TODO: Added model validation and remove below logic
                    if (!int.TryParse(totalCount, out actualTotalCount))
                        ModelState.AddModelError("TotalCountNonNumeric", "Value must be numeric!");
                    else if (actualTotalCount == 0)
                        ModelState.AddModelError("TotalCountZero", "Please enter value greater then zero!");
                    else
                    {
                        var pagedResults = this.fizzBuzzRepository.Get(actualTotalCount)
                                                                  .OrderBy(fb => fb.Value)
                                                                  .Skip((page - 1) * PageSize)
                                                                  .Take(PageSize);
                        var pagingInfo = new PagingInfo
                        {
                            CurrentPage = page,
                            ItemsPerPage = PageSize,
                            TotalItems = actualTotalCount,
                            PageSize = PageSize
                        };

                        var viewModel = new FizzBuzzViewModel
                        {
                            FizzBuzzEntries = pagedResults.ToList(),
                            PagingInfo = pagingInfo
                        };

                        return View(viewModel);
                    }
                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("error", ex.Message);
            }

            return View();

        }
    }
}