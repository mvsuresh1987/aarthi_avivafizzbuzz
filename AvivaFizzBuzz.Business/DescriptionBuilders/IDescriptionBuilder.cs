﻿namespace AvivaFizzBuzz.Business.DescriptionBuilders
{
    public interface IDescriptionBuilder
    {
        string GetDescription(int value);
    }
}
