// TODO: move using statements to below namespaces
using System;
using AvivaFizzBuzz.Business.DescriptionBuilders;

namespace AvivaFizzBuzz.Business
{
    public class FizzBuzzEntry : IFizzBuzzEntry
    {
        public int Value { get; private set; }

        public string Description { get; private set; }

        public FizzBuzzEntry(int value, IDescriptionBuilder builder)
        {
        // TODO: Remove validation in business layer.
            if (value < 1)
                throw new ArgumentOutOfRangeException("", "Specified value is out of the range of valid values and should be between 1 and 1000 !");

            if (value > 1000)
                throw new ArgumentOutOfRangeException("", "Specified value is out of the range of valid values and should be between 1 and 1000 !");

            if (builder == null)
                throw new ArgumentNullException("builder");

            this.Value = value;
            
            this.Description = builder.GetDescription(value);
        }
// TODO: remove overrride keyword
        public override string ToString()
        {
            return String.Format("{0}. {1}", this.Value, this.Description);
        } 
    }
}
