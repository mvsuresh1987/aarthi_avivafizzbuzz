﻿namespace AvivaFizzBuzz.Business
{
    public interface IFizzBuzzEntry
    {
        int Value { get; }

        string Description { get; }

        string ToString();
    }
}
