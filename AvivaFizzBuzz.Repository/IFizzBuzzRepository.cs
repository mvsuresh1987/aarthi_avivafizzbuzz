﻿using AvivaFizzBuzz.Business;
using System.Collections.Generic;

namespace AvivaFizzBuzz.Repository
{
    public interface IFizzBuzzRepository
    {
        IEnumerable<IFizzBuzzEntry> Get(int entriesToRetrieve);
    }
}
