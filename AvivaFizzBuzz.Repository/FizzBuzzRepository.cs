// TODO: Move using statements after namespace. Rename project name. Its not repository pattern
using System;
using System.Collections.Generic;
using AvivaFizzBuzz.Business;
using AvivaFizzBuzz.Business.DescriptionBuilders;

namespace AvivaFizzBuzz.Repository
{
    public class FizzBuzzRepository : IFizzBuzzRepository
    {
        private readonly IDescriptionBuilder descriptionBuilder;

        public FizzBuzzRepository(IDescriptionBuilder descriptionBuilder)
        {
        // TODO: Remove null check, If instance configuration not found structure map will triggre error.
            if (descriptionBuilder == null)
                throw new ArgumentNullException("descriptionBuilder");

            // Note: DescriptionBuilder is injected by the IOC Container: See AvivaFizzBuzz.Web.DependencyResolution.DefaultRegistry.
            this.descriptionBuilder = descriptionBuilder;
        }

        public IEnumerable<IFizzBuzzEntry> Get(int entriesToRetrieve)
        {
        // TODO: Move validation to model level.
            if (entriesToRetrieve < 0)
                throw new ArgumentOutOfRangeException("", "Please enter a positive integer!");

            for (var i = 1; i <= entriesToRetrieve; i++)
                yield return new FizzBuzzEntry(i, descriptionBuilder);
        }
    }
}
